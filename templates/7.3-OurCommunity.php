<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="hero-swiper swiper-wrapper">
		<div class="swiper"
			data-arrows="false"
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-2.jpg"></div>

				<div class="hero-content">
					<div class="hgroup">
						<h1 class="hgroup-title hero-title">Our Community</h1>
					</div><!-- .hgroup -->

					<span class="hero-subtitle">Morbi fermentum nibh eu neque aliquet pretium</span>
				</div><!-- .hero-content -->
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="nopad sw full">

		<div class="main-body">

			<div class="primary-sidebar o-first">

				<div class="sidebar-mod in-this-section-mod">
					<h3 class="mod-title">In This Section</h3>	

					<ul>
						<li class="selected"><a href="#">Our Community</a></li>
						<li><a href="#">News</a></li>
						<li><a href="#">Testimonials</a></li>
					</ul>
				</div><!-- .in-this-section-mod -->

			</div><!-- .primary-sidebar -->

			<div class="content">

				<div class="breadcrumbs">
					<div class="crumb-links">
						<a href="#" class="t-fa-abs fa-home">Home</a>
						<a href="#">The Latest</a>
						<a href="#">Our Community</a>
					</div><!-- .crumb-links -->
				</div><!-- .breadcrumbs -->

				<div class="article-body">
					
					<p>
						Phasellus eget ante lectus. Vivamus pellentesque iaculis laoreet. Nam facilisis felis ut diam porta 
						pellentesque. Sed a dui ac enim fermentum convallis. Praesent posuere in justo vitae consectetur. 
						In et nisl sagittis, semper magna id, blandit nulla. Donec accumsan magna vel quam sodales, vitae 
						varius nulla elementum. Proin vulputate lobortis nibh nec pharetra. Aenean nec massa non neque convallis 
						suscipit. Fusce vel enim id neque finibus maximus.
					</p>

					<p>
						Sed varius arcu pulvinar lorem faucibus bibendum. Cras convallis, neque et pretium fringilla, nulla nibh 
						egestas felis, vitae lobortis libero mi non nunc. Nullam faucibus finibus porttitor. Pellentesque nec 
						ultricies lacus. Suspendisse porttitor, diam ut cursus imperdiet, arcu nulla tempus lectus, at tempor 
						felis mi non turpis. Sed egestas elit sed aliquet sagittis. Sed id eros porta dolor feugiat faucibus. 
						Mauris lorem elit, facilisis vel egestas quis, bibendum vel mi.
					</p>

				</div><!-- .article-body -->
			</div><!-- .content -->

		</div><!-- .main-body -->

	</section>

	<section class="nopad lightest-bg">
		<div class="sw full">
			<div class="flex-results-wrap">

				<div class="filter-section">
					
					<div class="filter-bar">
						<div class="sw filter-bar-content">
						
							<div class="filter-bar-left">
								6 of 15 Stories
							</div>

							<div class="filter-bar-meta">

								<div class="filter-controls">
									<button class="previous">Previous</button>
									<button class="next">Next</button>
								</div>

							</div><!-- .filter-bar-meta -->

						</div><!-- .filter-bar-content -->
					</div><!-- .filter-bar -->

					<div class="filter-content">

						<div class="grid nopad card-grid collapse-599">

							<div class="col col-2">
								<a class="item card-item bounce" href="#">
									<div class="card-item-bg img lazybg" data-src="../assets/images/temp/overview-block-2.jpg"></div>
									<div class="card-item-content">
										<time datetime="2013-08-07">August 7, 2013</time>
										<span class="card-item-title">Food, Food, Food</span>
										<span class="button">Read More</span>
									</div><!-- .card-item-content -->
								</a><!-- .card-item -->
							</div><!-- .col -->

							<div class="col col-2">
								<a class="item card-item bounce" href="#">
									<div class="card-item-bg img lazybg" data-src="../assets/images/temp/overview-block-2.jpg"></div>
									<div class="card-item-content">
										<time datetime="2013-08-07">August 7, 2013</time>
										<span class="card-item-title">The Overhead Squat Will Punish Faulty Squat Form</span>
										<span class="button">Read More</span>
									</div><!-- .card-item-content -->
								</a><!-- .card-item -->
							</div><!-- .col -->

							<div class="col col-2">
								<a class="item card-item bounce" href="#">
									<div class="card-item-bg img lazybg" data-src="../assets/images/temp/overview-block-2.jpg"></div>
									<div class="card-item-content">
										<time datetime="2013-08-07">August 7, 2013</time>
										<span class="card-item-title">Happy Easter</span>
										<span class="button">Read More</span>
									</div><!-- .card-item-content -->
								</a><!-- .card-item -->
							</div><!-- .col -->

							<div class="col col-2">
								<a class="item card-item bounce" href="#">
									<div class="card-item-bg img lazybg" data-src="../assets/images/temp/overview-block-2.jpg"></div>
									<div class="card-item-content">
										<time datetime="2013-08-07">August 7, 2013</time>
										<span class="card-item-title">Easter Long Weekend Holiday Hours</span>
										<span class="button">Read More</span>
									</div><!-- .card-item-content -->
								</a><!-- .card-item -->
							</div><!-- .col -->

						</div><!-- .grid -->

					</div><!-- .filter-content -->

				</div><!-- .filter-section -->

				<div class="daily-results">

					<div>
						<h4 class="daily-results-title">Daily Results</h4>
						<time>April 9, 2015</time>

						<div class="selector with-arrow">
							<select>
								<option value="">Crossfit</option>
								<option value="">Option 2</option>
								<option value="">Option 3</option>
							</select>
							<span class="value"></span>
						</div><!-- .selector -->

						<div class="btn-group">
							<button>View</button>
							<button>Log</button>
							<button>Log</button>
						</div><!-- .btn-group -->

						<table>
							<tr>
								<td>Beaupre (5 sec) Hold</td>
								<td>Men</td>
								<td>Women</td>
							</tr>
						</table>

						<table>
							<tr>
								<td>Beaupre (5 sec) Hold</td>
								<td>Men</td>
								<td>Women</td>
							</tr>
						</table>

						<a href="#" class="button">Member Login</a>
					</div>

					<div>
						<h4 class="daily-results-title">Events &amp; Workshops</h4>

						<div class="event-info">
							<span class="event-info-title">What weakness 101 is all about!</span>
							<p>
								As a CrossFit athlete and coach, I know its hard to master all the things that come along with CrossFit. 
								Classes are awesome, they build community like no other fitness regime out there, plus they...
							</p>

							<a href="#" class="inline">More Details &raquo;</a>
						</div><!-- .event-info -->
						<a href="#" class="button">View All</a>
					</div>

				</div><!-- .daily-results -->

			</div><!-- .flex-results-wrap -->
		</div><!-- .sw.full -->
	</section>

	<?php include('inc/i-begin-fitness-journey.php'); ?>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>