<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="hero-swiper swiper-wrapper">
		<div class="swiper"
			data-arrows="false"
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-2.jpg"></div>

				<div class="hero-content">
					<div class="hgroup">
						<h1 class="hgroup-title hero-title">Contact</h1>
					</div><!-- .hgroup -->

					<span class="hero-subtitle">Morbi fermentum nibh eu neque aliquet pretium</span>
				</div><!-- .hero-content -->
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
			
			<div class="grid collapse-800">

				<div class="col col-2-5">
					<div class="item">
						
						<address>
							1980 Clark Dr. <br>
							Vancouver, BC V5N 0A9	
						</address>

						<br>
						<span class="block">+1 604 253 1261</span>
						<br>

						<?php include('inc/i-social.php'); ?>

					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3-5">

					<div class="item">
						<p>Fill out the form below to contact us.</p>
						<br>
						<form action="" class="body-form full">
							<div class="fieldset">

								<input type="text" name="name" placeholder="Full Name">
								<input type="email" name="email" placeholder="Email">
								<input type="text" name="subject" placeholder="Subject">
								<textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>

								<button class="button" type="submit">Contact Us</button>
							</div><!-- .fieldset -->
						</form>
					</div><!-- .item -->
				</div><!-- .col -->

			</div><!-- .grid -->

		</div><!-- .sw -->
	</section>

	<?php include('inc/i-contact-map.php'); ?>

	<?php include('inc/i-begin-fitness-journey.php'); ?>	

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>