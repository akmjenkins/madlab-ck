<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="hero-swiper swiper-wrapper">
		<div class="swiper"
			data-arrows="false"
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-2.jpg"></div>

				<div class="hero-content">
					<div class="hgroup">
						<h1 class="hgroup-title hero-title">Schedule</h1>
					</div><!-- .hgroup -->

					<span class="hero-subtitle">Morbi fermentum nibh eu neque aliquet pretium</span>
				</div><!-- .hero-content -->
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="nopad sw full">

		<div class="main-body">

			<div class="primary-sidebar o-first">

				<div class="sidebar-mod in-this-section-mod">
					<h3 class="mod-title">In This Section</h3>	

					<ul>
						<li><a href="#">Visit Crossfit Vancouver</a></li>
						<li><a href="#">Free Student Consultation</a></li>
						<li><a href="#">Starting Out</a></li>
						<li><a href="#">Fitness &amp; Injury</a></li>
						<li class="selected"><a href="#">Schedule</a></li>
						<li><a href="#">Fees</a></li>
						<li><a href="#">What Members Say</a></li>
					</ul>
				</div><!-- .in-this-section-mod -->

			</div><!-- .primary-sidebar -->

			<div class="content">

				<div class="breadcrumbs">
					<div class="crumb-links">
						<a href="#" class="t-fa-abs fa-home">Home</a>
						<a href="#">How To Join</a>
						<a href="#">Schedule</a>
					</div><!-- .crumb-links -->
				</div><!-- .breadcrumbs -->

				<div class="article-body">
					
					<p>
						Phasellus eget ante lectus. Vivamus pellentesque iaculis laoreet. Nam facilisis felis ut diam porta 
						pellentesque. Sed a dui ac enim fermentum convallis. Praesent posuere in justo vitae consectetur. 
						In et nisl sagittis, semper magna id, blandit nulla. Donec accumsan magna vel quam sodales, vitae 
						varius nulla elementum. Proin vulputate lobortis nibh nec pharetra. Aenean nec massa non neque convallis 
						suscipit. Fusce vel enim id neque finibus maximus.
					</p>

					<p>
						Sed varius arcu pulvinar lorem faucibus bibendum. Cras convallis, neque et pretium fringilla, nulla nibh 
						egestas felis, vitae lobortis libero mi non nunc. Nullam faucibus finibus porttitor. Pellentesque nec 
						ultricies lacus. Suspendisse porttitor, diam ut cursus imperdiet, arcu nulla tempus lectus, at tempor 
						felis mi non turpis. Sed egestas elit sed aliquet sagittis. Sed id eros porta dolor feugiat faucibus. 
						Mauris lorem elit, facilisis vel egestas quis, bibendum vel mi.
					</p>

				</div><!-- .article-body -->
			</div><!-- .content -->

		</div><!-- .main-body -->

	</section>

	<section class="lightest-bg pad40">
		<div class="sw full">
			<div class="grid eqh centered">

				<div class="col md-col-3 col-4 sm-col-2 xs-col-1">
					<a href="#" class="item with-btn bounce">

						<div class="coach-img-wrap">
							<div class="lazybg img" data-src="../assets/images/temp/overview-block-2.jpg"></div>
						</div><!-- .coach-img-wrap -->

						<span class="coach-title">Coach Name</span>

						<span class="btn-wrap">
							<span class="button">View Schedule</span>
						</span><!-- .btn-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col md-col-3 col-4 sm-col-2 xs-col-1">
					<a href="#" class="item with-btn bounce">

						<div class="coach-img-wrap">
							<div class="lazybg img" data-src="../assets/images/temp/overview-block-2.jpg"></div>
						</div><!-- .coach-img-wrap -->

						<span class="coach-title">Coach Name</span>

						<span class="btn-wrap">
							<span class="button">View Schedule</span>
						</span><!-- .btn-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col md-col-3 col-4 sm-col-2 xs-col-1">
					<a href="#" class="item with-btn bounce">

						<div class="coach-img-wrap">
							<div class="lazybg img" data-src="../assets/images/temp/overview-block-2.jpg"></div>
						</div><!-- .coach-img-wrap -->

						<span class="coach-title">Coach Name</span>

						<span class="btn-wrap">
							<span class="button">View Schedule</span>
						</span><!-- .btn-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col md-col-3 col-4 sm-col-2 xs-col-1">
					<a href="#" class="item with-btn bounce">

						<div class="coach-img-wrap">
							<div class="lazybg img" data-src="../assets/images/temp/overview-block-2.jpg"></div>
						</div><!-- .coach-img-wrap -->

						<span class="coach-title">Coach Name</span>

						<span class="btn-wrap">
							<span class="button">View Schedule</span>
						</span><!-- .btn-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col md-col-3 col-4 sm-col-2 xs-col-1">
					<a href="#" class="item with-btn bounce">

						<div class="coach-img-wrap">
							<div class="lazybg img" data-src="../assets/images/temp/overview-block-2.jpg"></div>
						</div><!-- .coach-img-wrap -->

						<span class="coach-title">Coach Name</span>

						<span class="btn-wrap">
							<span class="button">View Schedule</span>
						</span><!-- .btn-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col md-col-3 col-4 sm-col-2 xs-col-1">
					<a href="#" class="item with-btn bounce">

						<div class="coach-img-wrap">
							<div class="lazybg img" data-src="../assets/images/temp/overview-block-2.jpg"></div>
						</div><!-- .coach-img-wrap -->

						<span class="coach-title">Coach Name</span>

						<span class="btn-wrap">
							<span class="button">View Schedule</span>
						</span><!-- .btn-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col md-col-3 col-4 sm-col-2 xs-col-1">
					<a href="#" class="item with-btn bounce">

						<div class="coach-img-wrap">
							<div class="lazybg img" data-src="../assets/images/temp/overview-block-2.jpg"></div>
						</div><!-- .coach-img-wrap -->

						<span class="coach-title">Coach Name</span>

						<span class="btn-wrap">
							<span class="button">View Schedule</span>
						</span><!-- .btn-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col md-col-3 col-4 sm-col-2 xs-col-1">
					<a href="#" class="item with-btn bounce">

						<div class="coach-img-wrap">
							<div class="lazybg img" data-src="../assets/images/temp/overview-block-2.jpg"></div>
						</div><!-- .coach-img-wrap -->

						<span class="coach-title">Coach Name</span>

						<span class="btn-wrap">
							<span class="button">View Schedule</span>
						</span><!-- .btn-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

			</div><!-- .grid -->
		</div><!-- .sw.full -->
	</section>

	<?php include('inc/i-begin-fitness-journey.php'); ?>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>