<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="hero-swiper swiper-wrapper">
		<div class="swiper"
			data-arrows="false"
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-2.jpg"></div>

				<div class="hero-content">
					<div class="hgroup">
						<h1 class="hgroup-title hero-title">Testimonials</h1>
					</div><!-- .hgroup -->

					<span class="hero-subtitle">Morbi fermentum nibh eu neque aliquet pretium</span>
				</div><!-- .hero-content -->
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="nopad sw full">

		<div class="main-body">

			<div class="primary-sidebar o-first">

				<div class="sidebar-mod in-this-section-mod">
					<h3 class="mod-title">In This Section</h3>	

					<ul>
						<li><a href="#">Our Community</a></li>
						<li><a href="#">News</a></li>
						<li class="selected"><a href="#">Testimonials</a></li>
					</ul>
				</div><!-- .in-this-section-mod -->

			</div><!-- .primary-sidebar -->

			<div class="content">

				<div class="breadcrumbs">
					<div class="crumb-links">
						<a href="#" class="t-fa-abs fa-home">Home</a>
						<a href="#">The Latest</a>
						<a href="#">Testimonials</a>
					</div><!-- .crumb-links -->
				</div><!-- .breadcrumbs -->

				<div class="article-body">
					
					<p>
						Phasellus eget ante lectus. Vivamus pellentesque iaculis laoreet. Nam facilisis felis ut diam porta 
						pellentesque. Sed a dui ac enim fermentum convallis. Praesent posuere in justo vitae consectetur. 
						In et nisl sagittis, semper magna id, blandit nulla. Donec accumsan magna vel quam sodales, vitae 
						varius nulla elementum. Proin vulputate lobortis nibh nec pharetra. Aenean nec massa non neque convallis 
						suscipit. Fusce vel enim id neque finibus maximus.
					</p>

					<p>
						Sed varius arcu pulvinar lorem faucibus bibendum. Cras convallis, neque et pretium fringilla, nulla nibh 
						egestas felis, vitae lobortis libero mi non nunc. Nullam faucibus finibus porttitor. Pellentesque nec 
						ultricies lacus. Suspendisse porttitor, diam ut cursus imperdiet, arcu nulla tempus lectus, at tempor 
						felis mi non turpis. Sed egestas elit sed aliquet sagittis. Sed id eros porta dolor feugiat faucibus. 
						Mauris lorem elit, facilisis vel egestas quis, bibendum vel mi.
					</p>

				</div><!-- .article-body -->
			</div><!-- .content -->

		</div><!-- .main-body -->

	</section>

	<section class="nopad filter-section lightest-bg">
		<div class="filter-bar">
			<div class="sw filter-bar-content">
			
				<div class="filter-bar-left">
					4 of 15 Testimonials
				</div>

				<div class="filter-bar-meta">

					<div class="filter-controls">
						<button class="previous">Previous</button>
						<button class="next">Next</button>
					</div>

				</div><!-- .filter-bar-meta -->

			</div><!-- .filter-bar-content -->
		</div><!-- .filter-bar -->

		<div class="filter-content sw full">

			<div class="grid fill testimonial-grid nopad eqh collapse-850">

				<div class="col col-2">
					<div class="item pad-40 sm-pad-20">
						
						<blockquote class="testimonial">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
							Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
							tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
							Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus 
							sapien nunc eget odio.		

							<cite>Satisfied Customer</cite>					
						</blockquote><!-- .testimonial -->

					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-2">
					<div class="item pad-40 sm-pad-20">
						
						<blockquote class="testimonial">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
							Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
							tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
							Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus 
							sapien nunc eget odio.		

							<cite>Satisfied Customer</cite>					
						</blockquote><!-- .testimonial -->

					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-2">
					<div class="item pad-40 sm-pad-20">
						
						<blockquote class="testimonial">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
							Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
							tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
							Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus 
							sapien nunc eget odio.		

							<cite>Satisfied Customer</cite>					
						</blockquote><!-- .testimonial -->

					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col col-2">
					<div class="item pad-40 sm-pad-20">
						
						<blockquote class="testimonial">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
							Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar 
							tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
							Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus 
							sapien nunc eget odio.		

							<cite>Satisfied Customer</cite>					
						</blockquote><!-- .testimonial -->

					</div><!-- .item -->
				</div><!-- .col -->

			</div><!-- .grid -->

		</div><!-- .filter-content -->

	</section><!-- .filter-section -->

	<?php include('inc/i-begin-fitness-journey.php'); ?>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>