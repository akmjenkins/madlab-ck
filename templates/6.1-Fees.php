<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="hero-swiper swiper-wrapper">
		<div class="swiper"
			data-arrows="false"
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-2.jpg"></div>

				<div class="hero-content">
					<div class="hgroup">
						<h1 class="hgroup-title hero-title">Fees</h1>
					</div><!-- .hgroup -->

					<span class="hero-subtitle">Morbi fermentum nibh eu neque aliquet pretium</span>
				</div><!-- .hero-content -->
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="nopad sw full">

		<div class="main-body">

			<div class="primary-sidebar o-first">

				<div class="sidebar-mod in-this-section-mod">
					<h3 class="mod-title">In This Section</h3>	

					<ul>
						<li><a href="#">Visit Crossfit Vancouver</a></li>
						<li><a href="#">Free Student Consultation</a></li>
						<li><a href="#">Starting Out</a></li>
						<li><a href="#">Fitness &amp; Injury</a></li>
						<li><a href="#">Schedule</a></li>
						<li class="selected"><a href="#">Fees</a></li>
						<li><a href="#">What Members Say</a></li>
					</ul>
				</div><!-- .in-this-section-mod -->

			</div><!-- .primary-sidebar -->

			<div class="content">

				<div class="breadcrumbs">
					<div class="crumb-links">
						<a href="#" class="t-fa-abs fa-home">Home</a>
						<a href="#">How To Join</a>
						<a href="#">Fees</a>
					</div><!-- .crumb-links -->
				</div><!-- .breadcrumbs -->

				<div class="article-body">
					
					<p>
						Phasellus eget ante lectus. Vivamus pellentesque iaculis laoreet. Nam facilisis felis ut diam porta 
						pellentesque. Sed a dui ac enim fermentum convallis. Praesent posuere in justo vitae consectetur. 
						In et nisl sagittis, semper magna id, blandit nulla. Donec accumsan magna vel quam sodales, vitae 
						varius nulla elementum. Proin vulputate lobortis nibh nec pharetra. Aenean nec massa non neque convallis 
						suscipit. Fusce vel enim id neque finibus maximus.
					</p>

					<p>
						Sed varius arcu pulvinar lorem faucibus bibendum. Cras convallis, neque et pretium fringilla, nulla nibh 
						egestas felis, vitae lobortis libero mi non nunc. Nullam faucibus finibus porttitor. Pellentesque nec 
						ultricies lacus. Suspendisse porttitor, diam ut cursus imperdiet, arcu nulla tempus lectus, at tempor 
						felis mi non turpis. Sed egestas elit sed aliquet sagittis. Sed id eros porta dolor feugiat faucibus. 
						Mauris lorem elit, facilisis vel egestas quis, bibendum vel mi.
					</p>

				</div><!-- .article-body -->
			</div><!-- .content -->

		</div><!-- .main-body -->

	</section>

	<section class="nopad lightest-bg">
		<div class="tab-wrapper tabs-full">

			<div class="tab-controls dark-bg">
				<div class="sw full">
					<div class="selector with-arrow">
						<select class="tab-controller">
							<option>Apprentice Fees</option>
							<option>Regular Students</option>
							<option>About Our Fees</option>
							<option>Fast Track Apprentice Program</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->

					<div class="tab-control selected">
						Apprentice Fees
					</div>

					<div class="tab-control">
						Regular Students
					</div>

					<div class="tab-control">
						About Our Fees
					</div>

					<div class="tab-control">
						Fast Track Apprentice Program
					</div>
				</div><!-- .sw.full -->
			</div>
			<div class="tab-holder">
				
				<div class="tab selected article-body">
					<div class="sw">
						<div class="pad-40 sm-pad-10">
							

							<div class="hgroup centered">
								<h2 class="hgroup-title">In House Apprenticeship Diploma Program</h2>
							</div><!-- .hgroup -->

							<p class="centered">
								Fusce sagittis sagittis tristique. Mauris imperdiet, nisl placerat 
								iaculis tristique, quam odio elementum ipsum, id euismod odio ligula ut ligula.
							</p>

							<div class="grid center collapse-750 alternate">

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Junior Apprenticeship Program</strong>
										<small>9 - 18 Months</small>
										<strong class="primary block">$2040</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Associate Apprenticeship Program</strong>
										<small>18 - 24 Months</small>
										<strong class="primary block">$4080</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Senior Apprenticeship Program</strong>
										<small>12 - 24 Months</small>
										<strong class="primary block">$2040</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Fast Track Apprenticeship Program</strong>
										<small>4 Months</small>
										<strong class="primary block">$4500</strong>

									</div><!-- .item -->
								</div><!-- .col -->
							</div><!-- .grid -->

							<small class="block center">
								*All In House Prices listed are based on the maximum fee a student could incur. All prices subject to applicable taxes.
							</small>

						</div>
					</div><!-- .sw -->
				</div><!-- .tab -->

				<div class="tab article-body">
					<div class="sw">
						<div class="pad-40 sm-pad-10">
							

							<div class="hgroup centered">
								<h2 class="hgroup-title">Regular Students</h2>
							</div><!-- .hgroup -->

							<p class="centered">
								Fusce sagittis sagittis tristique. Mauris imperdiet, nisl placerat 
								iaculis tristique, quam odio elementum ipsum, id euismod odio ligula ut ligula.
							</p>

							<div class="grid center collapse-750 alternate">

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Junior Apprenticeship Program</strong>
										<small>9 - 18 Months</small>
										<strong class="primary block">$2040</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Associate Apprenticeship Program</strong>
										<small>18 - 24 Months</small>
										<strong class="primary block">$4080</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Senior Apprenticeship Program</strong>
										<small>12 - 24 Months</small>
										<strong class="primary block">$2040</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Fast Track Apprenticeship Program</strong>
										<small>4 Months</small>
										<strong class="primary block">$4500</strong>

									</div><!-- .item -->
								</div><!-- .col -->
							</div><!-- .grid -->

							<small class="block center">
								*All In House Prices listed are based on the maximum fee a student could incur. All prices subject to applicable taxes.
							</small>

						</div>
					</div><!-- .sw -->
				</div><!-- .tab -->

				<div class="tab article-body">
					<div class="sw">
						<div class="pad-40 sm-pad-10">
							

							<div class="hgroup centered">
								<h2 class="hgroup-title">About our Fees</h2>
							</div><!-- .hgroup -->

							<p class="centered">
								Fusce sagittis sagittis tristique. Mauris imperdiet, nisl placerat 
								iaculis tristique, quam odio elementum ipsum, id euismod odio ligula ut ligula.
							</p>

							<div class="grid center collapse-750 alternate">

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Junior Apprenticeship Program</strong>
										<small>9 - 18 Months</small>
										<strong class="primary block">$2040</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Associate Apprenticeship Program</strong>
										<small>18 - 24 Months</small>
										<strong class="primary block">$4080</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Senior Apprenticeship Program</strong>
										<small>12 - 24 Months</small>
										<strong class="primary block">$2040</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Fast Track Apprenticeship Program</strong>
										<small>4 Months</small>
										<strong class="primary block">$4500</strong>

									</div><!-- .item -->
								</div><!-- .col -->
							</div><!-- .grid -->

							<small class="block center">
								*All In House Prices listed are based on the maximum fee a student could incur. All prices subject to applicable taxes.
							</small>

						</div>
					</div><!-- .sw -->
				</div><!-- .tab -->

				<div class="tab article-body">
					<div class="sw">
						<div class="pad-40 sm-pad-10">
							
							<div class="hgroup centered">
								<h2 class="hgroup-title">Fast Track Apprenticeship Program</h2>
							</div><!-- .hgroup -->

							<p class="centered">
								Fusce sagittis sagittis tristique. Mauris imperdiet, nisl placerat 
								iaculis tristique, quam odio elementum ipsum, id euismod odio ligula ut ligula.
							</p>

							<div class="grid center collapse-750 alternate">

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Junior Apprenticeship Program</strong>
										<small>9 - 18 Months</small>
										<strong class="primary block">$2040</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Associate Apprenticeship Program</strong>
										<small>18 - 24 Months</small>
										<strong class="primary block">$4080</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Senior Apprenticeship Program</strong>
										<small>12 - 24 Months</small>
										<strong class="primary block">$2040</strong>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-2">
									<div class="item">
										
										<strong class="block">Fast Track Apprenticeship Program</strong>
										<small>4 Months</small>
										<strong class="primary block">$4500</strong>

									</div><!-- .item -->
								</div><!-- .col -->
							</div><!-- .grid -->

							<small class="block center">
								*All In House Prices listed are based on the maximum fee a student could incur. All prices subject to applicable taxes.
							</small>

						</div>
					</div><!-- .sw -->
				</div><!-- .tab -->

			</div><!-- .tab-holder -->
		</div><!-- .tab-wrapper -->
	</section>

	<?php include('inc/i-begin-fitness-journey.php'); ?>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>