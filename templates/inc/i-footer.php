			<footer>
			
				<div class="sw">
					<div class="footer-row">
					
						<div class="footer-address">
							<address>
								1980 Clark Dr. | Vancouver, BC | V5N 0A9 | +1 604 253 1261
							</address>
						</div><!-- .footer-address -->

						<div class="footer-copyright">
							Copyright &copy; <?php echo date('Y'); ?>
							<a href="#">Sitemap</a>
							<a href="#">Legal</a>
						</div><!-- .footer-copyright -->

						<div class="footer-jac">
							<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/images/jac-logo.svg" alt="JAC Logo."></a>							
						</div><!-- .footer-jac -->

					</div><!-- .footer-row -->
				</div><!-- .sw -->
				
			</footer>

		</div><!-- .page-wrapper -->
		
		<?php include('i-global-search-form.php'); ?>

		<script>
			var templateJS = {
				templateURL: 'http://dev.me/madlab-ck',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>

		<script src="../assets/js/min/main-min.js"></script>
	</body>
</html>