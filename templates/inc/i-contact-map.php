	<section class="nopad">
		<div class="contact-map-wrap">
			<div class="contact-map"></div>

			<div class="sw">
				<div class="contact-info">
				
					<div class="lazybg ib with-img map-marker-placeholder">
						<img src="../assets/images/marker.svg" alt="map marker">
					</div><!-- .lazybg -->
					
					<address>
						<span>1980 Clark Dr.</span>
						Vancouver, BC V5N 0A9
					</address>
					
					<?php include('i-social.php'); ?>
				
				</div><!-- .contact-info -->	
			</div><!-- .sw -->
		</div><!-- .contact-map-wrap -->
	</section><!-- .nopad -->