	<section class="d-bg textured-bg center">
		<div class="sw">

			<div class="hgroup centered">
				<h3 class="hgroup-title">Begin Your Fitness Journey</h3>
			</div><!-- .hgroup -->

			<p class="centered">
				Sign up today to start on the road to becoming a pro.
			</p>

			<a href="#" class="button dark big">Get Started</a>

		</div><!-- .sw -->
	</section><!-- .textured-bg -->