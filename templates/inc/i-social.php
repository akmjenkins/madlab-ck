<div class="social">
	<a href="#" title="Like MadLab on Facebook" class="social-fb" rel="external">Like MadLab on Facebook</a>
	<a href="#" title="Follow MadLab on Twitter" class="social-tw" rel="external">Follow MadLab on Twitter</a>
	<a href="#" title="Add MadLab to your circles on Google Plus" class="social-gp" rel="external">Add MadLab to your circles on Google Plus</a>
	<a href="#" title="Check out MadLab on Instagram" class="social-ig" rel="external">Check out MadLab on Instagram</a>
	<a href="#" title="Subscribe to MadLab's YouTube Channel" class="social-yt" rel="external">Subscribe to MadLab's YouTube Channel</a>
</div><!-- .social -->