	<section class="d-bg lazybg testimonial-section" data-src="../assets/images/temp/full-bg-1.jpg">
		<div class="swiper-wrapper">
			<div class="swiper" id="testimonial-swiper"
				data-arrows="false"
				data-as-nav-for="#testimonial-image-swiper">

				<div class="swipe-item">

					<blockquote class="testimonial">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
						Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. 

						<cite>Jane Doe</cite>

					</blockquote>
				</div><!-- .swipe-item -->

				<div class="swipe-item">

					<blockquote class="testimonial">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
						Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. 

						<cite>Jane Doe</cite>

					</blockquote>
				</div><!-- .swipe-item -->

			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->

		<div class="testimonial-image-wrapper">
			<div class="testimonial-image-swiper swiper-wrapper">
				<div class="swiper" id="testimonial-image-swiper"
					data-fade="true"
					data-arrows="true"
					data-as-nav-for="#testimonial-swiper"
					data-update-lazy-images="true">
					<div class="swipe-item">
						<div class="swipe-item-bg" data-src="../assets/images/temp/overview-block-2.jpg"></div>
					</div><!-- .swipe-item -->
					<div class="swipe-item">
						<div class="swipe-item-bg" data-src="../assets/images/temp/overview-block-1.jpg"></div>
					</div><!-- .swipe-item -->
				</div><!-- .swiper -->
			</div><!-- .swiper-wrapper -->

			<svg class="clip-svg">
				  <defs>
					    <clipPath id="clip-diamond-demo" clipPathUnits="objectBoundingBox">
						      <polygon points="0.5 0, 1 0.5, 0.5 1, 0 0.5" />
					    </clipPath>
				  </defs>	
			</svg>

		</div><!-- .testimonial-image-wrapper -->

	</section>