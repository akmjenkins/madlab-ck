<div class="nav-meta">
	<div class="sw">
		
		<a href="#">Login</a>
		<a href="#">Location</a>
		<a href="#">Contact</a>

	</div><!-- .sw -->
</div><!-- .nav-meta -->

<div class="nav-overlay">

	<div class="sw">
		<button class="toggle-nav">Close</button>
	</div>

	<nav>
		<ul>
			<li><a href="#">What is Crossfit?</a></li>
			<li><a href="#">MLG Prescription for a Great Life</a></li>
			<li><a href="#">Who We Are</a></li>
			<li><a href="#">How to Join</a></li>
			<li><a href="#">The Latest</a></li>
		</ul>

		<?php include('i-social.php'); ?>
	</nav>

</div><!-- .nav-overlay -->