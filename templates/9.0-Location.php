<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="hero-swiper swiper-wrapper">
		<div class="swiper"
			data-arrows="false"
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-2.jpg"></div>

				<div class="hero-content">
					<div class="hgroup">
						<h1 class="hgroup-title hero-title">Location</h1>
					</div><!-- .hgroup -->

					<span class="hero-subtitle">Morbi fermentum nibh eu neque aliquet pretium</span>
				</div><!-- .hero-content -->
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="nopad">
		<div class="location-map">
			
			<div class="location-selector-wrap">
				<div class="selector with-arrow">
					<select>

						<option
							data-center="49.2672079,-123.077251"
							data-zoom="17"
						>1980 Clark Drive</option>

						<option 
							data-center="47.5242761,-52.7721886" 
							data-zoom="17">673 Topsail Road</option>
							
					</select>
					<span class="value">&nbsp;</span>
				</div><!-- .selector -->
			</div><!-- .location-selector-wrap -->

			<div class="gmap">
				<div class="map" 

					data-center="49.2672079,-123.077251" 
					data-zoom="17" 
					data-markers='<?php 
						echo json_encode(array(
								array(
									"position" => "49.2672079,-123.077251"
								)
							)); 
					?>'></div>
			</div><!-- .gmap -->

		</div><!-- .tab-wrapper -->
	</section>

	<?php include('inc/i-begin-fitness-journey.php'); ?>	

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>