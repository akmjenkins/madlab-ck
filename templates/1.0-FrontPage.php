<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="full-video">
</div>

<div class="hero">
	
	<div class="hero-swiper swiper-wrapper">
		<div class="swiper"
			data-arrows="false" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/home-hero.svg"></div>

				<div class="hero-content">
					<div class="hgroup">
						<h1 class="hgroup-title hero-title">Fit For Life</h1>
					</div><!-- .hgroup -->

					<div>
						<a href="http://www.weathershorewindows.com/wp-content/themes/core-theme/assets/video/Web-Vid-One.mp4" 
							data-type="video" 
							data-mpopup-opts='<?php echo json_encode(array("mainClass"=>'mfp-fs-video')); ?>' 
							class="mpopup button t-fa fa-play-circle">Play Video</a>
					</div>
				</div><!-- .hero-content -->

			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">

			<div class="excerpt-section">

				<p class="centered">
					We train you to become functionally fit for life, and our in-depth apprentice coaching 
					diploma programs turn aspiring coaches into health and fitness experts.
				</p>

				<a href="#" class="button">Get Started</a>
			</div><!-- .excerpt-section -->


		</div><!-- .sw -->
	</section>

	<section class="d-bg textured-bg">
		<div class="sw">

			<div class="hgroup centered">
				<h3 class="hgroup-title">We Help You Reach Your Fitness Goals</h3>
			</div><!-- .hgroup -->

			<p class="centered">
				We have spent 10 years learning about what works and what doesn’t work in terms of getting people fit, 
				and perhaps more importantly, we have learned what works in order to keep people motivated and 
				injury-free so they can stay fit for life.
			</p>

			<br>

			<div class="grid eqh center collapse-850">
				<div class="col col-3">
					<div class="item bounce with-ico with-btn">
						
						<span class="img ico custom-f-abs ml-dumbbell">Dumbbell</span>
						<h4>What is CrossFit?</h4>

						<span class="btn-wrap">
							<a href="#" class="button">Find Out More</a>
						</span>

					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3">
					<div class="item bounce with-ico with-btn">

						<span class="img ico custom-f-abs ml-heart">Heart</span>
						<h4>MLG Prescription for a Great Life</h4>

						<span class="btn-wrap">
							<a href="#" class="button">Find Out More</a>
						</span>
						
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3">
					<div class="item bounce with-ico with-btn">

						<span class="img ico custom-f-abs ml-faq">Question</span>
						<h4>Who We Are</h4>

						<span class="btn-wrap">
							<a href="#" class="button">Find Out More</a>
						</span>
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->

		</div><!-- .sw -->
	</section><!-- .textured-bg -->

	<?php include('inc/i-testimonials.php'); ?>

	<section>
		<div class="sw">
			
			<div class="centered">
				<h3>Interest in MadLab School of Fitness?</h3>

				<p>Get in touch with us today for your free student consultation.</p>

				<a href="#" class="button big">Get Started</a>
			</div><!-- .centered -->

		</div><!-- .sw -->
	</section>


	<?php include('inc/i-contact-map.php'); ?>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>