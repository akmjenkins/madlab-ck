<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="hero-swiper swiper-wrapper">
		<div class="swiper"
			data-arrows="false"
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-2.jpg"></div>

				<div class="hero-content">
					<div class="hgroup">
						<h1 class="hgroup-title hero-title">Affiliate Network</h1>
					</div><!-- .hgroup -->

					<span class="hero-subtitle">Morbi fermentum nibh eu neque aliquet pretium</span>
				</div><!-- .hero-content -->
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="lightest-bg">
		<div class="sw">
			
			<div class="grid eqh collapse-850">

				<div class="col col-2">
					<div class="item pad-40 sm-pad-10">
						
						<h4>Existing User Login</h4>

						<form action="" class="body-form full">

							<div class="fieldset grid pad10">
								<div class="col col-1">
									<div class="item">
										<input type="text" name="username" placeholder="Username">		
									</div>
								</div>
								<div class="col col-1">
									<div class="item">
										<input type="password" name="password" placeholder="Password">
									</div>
								</div>
								<div class="col col-1">
									<div class="item">
										<label>
											<input type="checkbox" name="remember"> Remember me
										</label>
										<a href="#" class="inline f-right">Forgot your password?</a>
									</div>
								</div>
							</div><!-- .fieldset -->

							<button class="button" type="submit">Login</button>
						</form><!-- .body-form -->

					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col-separator">&nbsp;</div>

				<div class="col col-2">
					<div class="item pad-40 sm-pad-10">

						<h4>New User Sign Up</h4>

						<form action="" class="body-form full">

							<div class="fieldset grid pad10">
								<div class="col col-1">
									<div class="item">
										<input type="text" name="username" placeholder="Username">
									</div>
								</div>
								<div class="col col-1">
									<div class="item">
										<input type="email" name="email" placeholder="Email">
									</div>
								</div>
								<div class="col col-1">
									<div class="item">
										<input type="password" name="password" placeholder="Password">
									</div>
								</div>
								<div class="col col-1">
									<div class="item">
										<input type="password" name="confirm_password" placeholder="Confirm Password">
									</div>
								</div>
							</div><!-- .fieldset -->
							<button class="button" type="submit">Login</button>
						</form><!-- .body-form -->

					</div><!-- .item -->
				</div><!-- .col -->

			</div><!-- .grid -->

		</div><!-- .sw -->
	</section>

	<?php include('inc/i-begin-fitness-journey.php'); ?>	

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>