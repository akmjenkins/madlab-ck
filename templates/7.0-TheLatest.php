<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="hero-swiper swiper-wrapper">
		<div class="swiper"
			data-arrows="false"
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-2.jpg"></div>

				<div class="hero-content">
					<div class="hgroup">
						<h1 class="hgroup-title hero-title">The Latest</h1>
					</div><!-- .hgroup -->

					<span class="hero-subtitle">Morbi fermentum nibh eu neque aliquet pretium</span>
				</div><!-- .hero-content -->
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="nopad">

		<div class="section-header">
			<div class="sw">
				<h4 class="section-header-title">Latest News</h4>
				<a href="#" class="button">View All</a>
			</div><!-- .sw -->
		</div><!-- .section-header -->

		<div class="darkest-bg">
			<div class="sw full">
				<div class="grid nopad card-grid collapse-750">

					<div class="col col-3">
						<a class="item card-item bounce" href="#">
							<div class="card-item-bg img lazybg" data-src="../assets/images/temp/overview-block-2.jpg"></div>
							<div class="card-item-content">
								<time datetime="2013-08-07">August 7, 2013</time>
								<span class="card-item-title">Rachel Clifford gets her life on track in Vancouver</span>
								<span class="button">Read More</span>
							</div><!-- .card-item-content -->
						</a><!-- .card-item -->
					</div><!-- .col -->

					<div class="col col-3">
						<a class="item card-item bounce" href="#">
							<div class="card-item-bg img lazybg" data-src="../assets/images/temp/overview-block-2.jpg"></div>
							<div class="card-item-content">
								<time datetime="2013-08-07">August 7, 2013</time>
								<span class="card-item-title">Rachel Clifford gets her life on track in Vancouver</span>
								<span class="button">Read More</span>
							</div><!-- .card-item-content -->
						</a><!-- .card-item -->
					</div><!-- .col -->

					<div class="col col-3">
						<a class="item card-item bounce" href="#">
							<div class="card-item-bg img lazybg" data-src="../assets/images/temp/overview-block-2.jpg"></div>
							<div class="card-item-content">
								<time datetime="2013-08-07">August 7, 2013</time>
								<span class="card-item-title">Rachel Clifford gets her life on track in Vancouver</span>
								<span class="button">Read More</span>
							</div><!-- .card-item-content -->
						</a><!-- .card-item -->
					</div><!-- .col -->

				</div><!-- .grid -->
			</div><!-- .sw.full -->
		</div><!-- .darkest-bg -->
	</section><!-- .nopad -->


	<section class="nopad">
		<div class="section-header">
			<div class="sw">
				<h4 class="section-header-title">Testimonials</h4>
				<a href="#" class="button">View All</a>
			</div><!-- .sw -->
		</div><!-- .section-header -->
	</section><!-- .nopad -->

	<?php include('inc/i-testimonials.php'); ?>

	<?php include('inc/i-begin-fitness-journey.php'); ?>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>