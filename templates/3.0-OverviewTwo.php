<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="hero-swiper swiper-wrapper">
		<div class="swiper"
			data-arrows="false"
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-2.jpg"></div>

				<div class="hero-content">
					<div class="hgroup">
						<h1 class="hgroup-title hero-title">What is Crossfit?</h1>
					</div><!-- .hgroup -->

					<span class="hero-subtitle">Morbi fermentum nibh eu neque aliquet pretium</span>
				</div><!-- .hero-content -->
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">

			<div class="overview-excerpt-section article-body">

				<p>
					Fusce accumsan vel augue vel tincidunt. Donec ut quam auctor, iaculis nisl ut, ullamcorper libero. 
					Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur 
					efficitur eros in ante tincidunt, sit amet luctus tellus cursus. Aliquam bibendum bibendum nibh, 
					ac pellentesque velit porta eu. Aenean semper nisi eget nisi maximus, nec rutrum enim ultrices. 
					Suspendisse facilisis erat sed quam rhoncus scelerisque. Mauris in interdum metus.
				</p>

				<p>
					Phasellus eget ante lectus. Vivamus pellentesque iaculis laoreet. Nam facilisis felis ut diam porta pellentesque. 
					Sed a dui ac enim fermentum convallis. Praesent posuere in justo vitae consectetur. In et nisl sagittis, 
					semper magna id, blandit nulla. Donec accumsan magna vel quam sodales, vitae varius nulla elementum. 
					Proin vulputate lobortis nibh nec pharetra. Aenean nec massa non neque convallis suscipit. Fusce vel enim id 
					neque finibus maximus.
				</p>


			</div><!-- .overview-excerpt-section -->
		</div><!-- .sw -->
	</section>

	<section class="nopad lightest-bg">

			<div class="ov-block article-body">

				<div class="ov-block-img">
					<div class="lazybg" data-src="../assets/images/temp/overview-block-1.jpg"></div>	
				</div><!-- .ov-block-img -->

				<div class="ov-block-content">
					
					<div class="hgroup">
						<h3>The Movements</h3>
					</div><!-- .hgroup -->

					<p>
						Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur efficitur 
						eros in ante tincidunt, sit amet luctus tellus cursus. Aliqua bibendum bibendum nibh, ac pellentesque 
						velit porta eu.					
					</p>

					<a href="#" class="button">Find Out More</a>

				</div><!-- .ov-block-content -->

			</div><!-- .ov-block -->

			<div class="ov-block article-body">

				<div class="ov-block-img">
					<div class="lazybg" data-src="../assets/images/temp/overview-block-1.jpg"></div>	
				</div><!-- .ov-block-img -->

				<div class="ov-block-content">
					
					<div class="hgroup">
						<h3>Affiliate Network</h3>
					</div><!-- .hgroup -->

					<p>
						Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur efficitur 
						eros in ante tincidunt, sit amet luctus tellus cursus. Aliqua bibendum bibendum nibh, ac pellentesque 
						velit porta eu.					
					</p>

					<a href="#" class="button">Find Out More</a>

				</div><!-- .ov-block-content -->
				
			</div><!-- .ov-block -->

			<div class="ov-block article-body">

				<div class="ov-block-img">
					<div class="lazybg" data-src="../assets/images/temp/overview-block-1.jpg"></div>	
				</div><!-- .ov-block-img -->

				<div class="ov-block-content">
					
					<div class="hgroup">
						<h3>How to Choose a Gym</h3>
					</div><!-- .hgroup -->

					<p>
						Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur efficitur 
						eros in ante tincidunt, sit amet luctus tellus cursus. Aliqua bibendum bibendum nibh, ac pellentesque 
						velit porta eu.					
					</p>

					<a href="#" class="button">Find Out More</a>

				</div><!-- .ov-block-content -->
				
			</div><!-- .ov-block -->

			<div class="ov-block article-body">

				<div class="ov-block-img">
					<div class="lazybg" data-src="../assets/images/temp/overview-block-1.jpg"></div>	
				</div><!-- .ov-block-img -->

				<div class="ov-block-content">
					
					<div class="hgroup">
						<h3>MadLab Group</h3>
					</div><!-- .hgroup -->

					<p>
						Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur efficitur 
						eros in ante tincidunt, sit amet luctus tellus cursus. Aliqua bibendum bibendum nibh, ac pellentesque 
						velit porta eu.					
					</p>

					<a href="#" class="button">Find Out More</a>

				</div><!-- .ov-block-content -->
				
			</div><!-- .ov-block -->
	</section><!-- .nopad -->

	<?php include('inc/i-begin-fitness-journey.php'); ?>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>