;(function(context) {

	var tests;
	var debounce;
	var imageLoader;
	var d;
	var swiper;
	
	if(context) {
		debounce = context.debounce;
		tests = context.tests;
		imageLoader = context.imageLoader;
		swiper = context.swiper;
	} else {
		debounce = require('./scripts/debounce.js');
		tests = require('./scripts/tests.js');
		imageLoader = require('./scripts/image.loader.js');
		swiper = require('./scripts/swiper.js');
	}
	
	d = debounce();

	//hero video
	(function() {
		var hasCreatedVideo = false;
		var $document = $(document);
		var resizeDebounce = debounce();

		var methods = {
			doResize: function() {

				if(hasCreatedVideo || window.innerWidth < 800) { return; }

				hasCreatedVideo = true;

				$('.full-video').videoBG({
					mp4: 'http://www.weathershorewindows.com/wp-content/themes/core-theme/assets/video/Web-Vid-One.mp4',
					ogv: 'http://www.weathershorewindows.com/wp-content/themes/core-theme/assets/video/Web-Vid-One.ogv',
					webm: 'http://www.weathershorewindows.com/wp-content/themes/core-theme/assets/video/Web-Vid-One.webm',
					//do not set a poster, it doesn't look great
					//poster		: templateJS.templateURL + '/assets/images/temp/section-bg.jpg',
					scale: true,
					zIndex: 0,
					width: '100%',
					height: '100%'
				});
			}
		};

		$document
			.on('ready',function() { 
				$(window)
					.on('resize',function() {
						d.requestProcess(function() { methods.doResize(); });
					})
					.trigger('resize')
			});

	}());

	$('.location-map').each(function() {
		var 
			map,
			$el = $(this),
			$selector = $('select',$el);

		$el.on('mapLoad',function(e,mapObj) { map = mapObj; });

		$selector
			.on('change',function() {
				var 
					$option = $(this).find('option').eq(this.selectedIndex),
					d = $option.data(),
					pos = d.center.split(','),
					c = new google.maps.LatLng(+pos[0],+pos[1]);

				map.map.setZoom(d.zoom);
				map.map.setCenter(c);
				map.markers[0].setPosition(c);
			})


	});
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));