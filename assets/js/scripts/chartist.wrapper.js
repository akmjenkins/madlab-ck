;(function(context) {


	var methods;

	var CHART_TYPES = {
		CHART_TYPE_PIE: 'pie',
		CHART_TYPE_BAR: 'bar'
	};

	methods = {

		generateRandomId: function() {
			return "a"+Math.ceil(Math.random(1,100)*100)*Date.now();
		},

		getChartElements: function() {
			return $('.chart').filter(function() { return !$(this).hasClass('charted')});
		},

		doCharts: function() {
			var self = this;
			this.getChartElements().each(function() {
				var 
					$el = $(this),
					d = $el.data(),
					chart = d.chartistChart;


				if(chart) {
					return chart.update();
				}

				//every chart element needs an id
				if(!$el[0].id) {
					$el.attr('id',self.generateRandomId());
				}

				if(!d.chartType) {
					throw new Error('Chart type for not specified for '+$el);
					return;
				}

				switch(d.chartType.toLowerCase()) {
					case CHART_TYPES.CHART_TYPE_PIE:
						self.doPieChart($el,d);
						break;
					case CHART_TYPES.CHART_TYPE_BAR:
						self.doBarChart($el,d);
						break;
				}


			});
		},

		doBarChart: function($el,data) {
			$el.data('chartistChart',new Chartist.Bar('#'+$el[0].id,data.chartData,data.chartOptions));
		},

		doPieChart: function($el,data) {
			$el.data('chartistChart',Chartist.Pie('#'+$el[0].id,data.chartData,data.chartOptions));
			var legendString = '<ul class="chartist-pie-legend">';
			data.chartData.labels.forEach(function(label,i) {
				legendString += '<li class="ct-series'+i+'">'+label+'</li>';
			});

			legendString += '</ul>';

			$el.parent().append(legendString);
		}
	};

	$(document).on('updateTemplate.charts',function() {
		methods.doCharts();
		console.log('here');
	}).trigger('updateTemplate.charts');

}(typeof ns !== 'undefined' ? window[ns] : undefined));