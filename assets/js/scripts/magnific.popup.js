;(function(context) {

	var debounce;
	
	if(context) {
		debounce = context.debounce;
	} else {
		debounce = require('./debounce.js');
	}

	var d = debounce();

		var getMagnificItemForEl = function(el) {
			var 
				magnificItem = {},
				type = el.data('type');
				magnificItem.src = el.data('src') || el[0].href;
				magnificItem.title = el.data('title');
				
				//a little help
				if(magnificItem.src.match(/vimeo|youtube|maps/i)) {
					type = 'iframe';
				} else if(magnificItem.src.match(/(jpg|gif|png|jpeg)(\?.*)?$/i)) {
					type = 'image';
				}				
				
			switch(type) {
				case 'image':
					magnificItem.type = 'image';
					break;
				case 'inline':
					magnificItem.type = 'inline';
					break;
				case 'ajax':
					magnificItem.type = 'ajax';
					break;
				case 'iframe':
				case 'video':
				case 'map':
					magnificItem.type = 'iframe';
					break;
			}

			return magnificItem;
		};

		//magnific popup
		$(document)
			.on('click','.mpopup-close',function() {
				$.magnificPopup.close();
			})
			.on('click','.mpopup',function(e) {
				e.preventDefault();
				var 
					magnificItems,
					items,
					isPartOfGallery,
					thisIndex = 0,
					el = $(this),
					galleryName = el.data('gallery'),
					mpopupOptions = el.data('mpopupOpts') || {};
					
					isPartOfGallery = !!galleryName;

					console.log(mpopupOptions);
				
				if(isPartOfGallery) {
					magnificItems = [];
					items = $('.mpopup').filter(function(i) { return $(this).data('gallery') === galleryName; });
					
					items.each(function(i) { 
						if(this === el[0]) { 
							thisIndex = i;  
						} 
						magnificItems.push(getMagnificItemForEl($(this)));
						return true; 
					});
				} else {
					magnificItems = getMagnificItemForEl(el);
				}
				
				$.magnificPopup.open($.extend({
					items:magnificItems,
					disableOn: 700,
					mainClass: 'mfp-zoom-in',
					removalDelay: 160,
					preloader: true,
					fixedContentPos: true,
					gallery:{enabled:isPartOfGallery},
					callbacks: {
						open: function() {
							$(document).trigger('updateTemplate');
							$('.mfp-wrap').on('scroll',function() {
								d.requestProcess(function() { $(document).trigger('updateTemplate.lazyimages'); }); 								
							})
						},
						markupParse: function() {
							$(document).trigger('updateTemplate');
						}
					}
				},mpopupOptions),thisIndex);
			
			});

}(typeof ns !== 'undefined' ? window[ns] : undefined));