;(function(context) {

	var gmap;
	var HTMLMarker;
	var $window;
	var debounce;
	var d;
	var contactMap;
	
	if(context) {
		gmap = context.gmap;
		HTMLMarker = context.HTMLMarker;
		debounce = context.debounce;
	} else {
		gmap = require('./gmap.js');
		HTMLMarker = require('./map/html.marker.js');
		debounce = require('./debounce.js');
	}
	
	d = debounce();
	$window = $(window);
	
	
	var ContactMap = function($el) {
		var self = this;
		
		this.$mapEl = $el;
		
		this
			.$mapEl
			.data('center','0,0')
			.on('mapLoad',function(e,map) { self.onMapLoad(map); });
		
		gmap.buildMap(this.$mapEl,
			{
				zoom: ContactMap.zoom,
				minZoom: ContactMap.zoom,
				maxZoom: ContactMap.zoom,
				zoomControl: false,
				scrollwheel:false,
				draggable:false,
				center: {lat:52,lng:52}
			}
		);
		
	};
	
	ContactMap.center = {
		lat:49.2672079,
		lng:-123.077251
	};
	
	ContactMap.zoom = 17;
	
	ContactMap.prototype.getMap = function() {
		return this.map ? this.map.map : null;
	};
	
	ContactMap.prototype.doCenter = function() {
		var 
			currentCenter,
			newCenter,
			self = this,
			projection = this.marker.getProjection(),
			$marker = $('.map-marker-placeholder'),
			mapRect = this.$mapEl[0].getBoundingClientRect(),
			markerRect = $marker[0].getBoundingClientRect(),
			offsetFromCenter = {
				x: (markerRect.left+markerRect.width/2) - (mapRect.left + mapRect.width/2),
				y: (markerRect.top+markerRect.height) - (mapRect.top + mapRect.height/2)
			};
			
			if(!projection) { return; }
			
			currentCenter = projection.fromLatLngToDivPixel(this.marker.getPosition()),
			newCenter = projection.fromDivPixelToLatLng(new google.maps.Point( (currentCenter.x - offsetFromCenter.x), (currentCenter.y - offsetFromCenter.y )));
			this.getMap().setCenter(newCenter);
			
		self.$mapEl.addClass('ready');
	};
	
	ContactMap.prototype.onMapLoad = function(map) {
		var self = this;
		this.map = map;
		
		this.getMap().setCenter(ContactMap.center);
		this.getMap().setZoom(ContactMap.zoom);
		
		//add an HTMLMarker to the map
		this.marker = HTMLMarker({
			position: new google.maps.LatLng(ContactMap.center.lat,ContactMap.center.lng),
			map:this.getMap(),
			'class':'madlab-marker',
			onAdd: function() { self.doCenter(); }
		});
		
	};
	
	$('.contact-map').each(function() {
		contactMap = new ContactMap($(this));
		$window.on('resize',function() { d.requestProcess(function() { contactMap.doCenter(); }); })
	});
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));