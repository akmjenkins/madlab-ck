;(function(context) {

	var $body = $('body');

	$.fn.scrollTo = function( target, options, callback ){

		if(typeof options == 'function' && arguments.length == 2){ 
			callback = options; options = target; 
		}
		
		var settings = $.extend({
			scrollTarget: target,
			offsetTop: 50,
			duration: 500,
			easing: 'swing'
		}, options);
		
		return this.each(function(){
			var scrollPane = $(this);
			var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
			var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top - parseInt(settings.offsetTop);
			scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
				if (typeof callback == 'function') { callback.call(this); }
			});
		});
	}
	
	//CommonJS
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = function() { 
			$body.scrollTo.apply($body,arguments);
		};
	//CodeKit
	} else if(context) {
		context.scrollTo = function() {
			$body.scrollTo.apply($body,arguments);
		}
	}

}(typeof ns !== 'undefined' ? window[ns] : undefined));